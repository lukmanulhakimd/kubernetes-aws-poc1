#!/bin/bash

set -o nounset
set -o pipefail

controller_up() {
  echo ""
  echo "############################"
  echo "Creating controller stack.."
  echo "> aws cloudformation create-stack --tag 'Key=pic,Value=$PIC' 'Key=KubernetesCluster,Value=$KUBE_CLUSTER_NAME' --stack-name ${KUBE_CLUSTER_NAME}-controller --template-body file://controller.yml"
  aws cloudformation create-stack --tag "Key=pic,Value=$PIC" "Key=KubernetesCluster,Value=$KUBE_CLUSTER_NAME" --stack-name ${KUBE_CLUSTER_NAME}-controller --template-body file://controller.yml
}

worker_group1_up() {
  echo ""
  echo "############################"
  echo "Creating worker group-1 stack.."
  echo "> aws cloudformation create-stack --tag 'Key=pic,Value=$PIC' 'Key=KubernetesCluster,Value=$KUBE_CLUSTER_NAME' --stack-name '${KUBE_CLUSTER_NAME}-worker-group1' --template-body file://worker_group1.yml"
  aws cloudformation create-stack --tag "Key=pic,Value=$PIC" "Key=KubernetesCluster,Value=$KUBE_CLUSTER_NAME" --stack-name ${KUBE_CLUSTER_NAME}-worker-group1 --template-body file://worker_group1.yml
}



component=$1
#if [[ "${component}" == "controller_up" ]]; then
#  echo "Creating controller nodes.."
#  controller_up
#elif [[ "${component}" == "worker_group1_up" ]]; then
#  echo "Creating controller nodes.."
#  worker_group1_up
#elif [[ "${component}" == "" ]]; then
#  echo ""
#else
#  echo "${component} provision is not supported!"
#fi

#aws ec2 describe-instances --filters "Name=tag:aws:cloudformation:stack-name,Values=${KUBE_CLUSTER_NAME}-${component}" --query "Reservations[*].Instances[*].InstanceId[]" | jq .
aws ec2 describe-instances --filters "Name=tag:aws:cloudformation:stack-name,Values=${KUBE_CLUSTER_NAME}-${component}"  | jq '.Reservations[].Instances[] | .PublicIpAddress + " " + .PrivateIpAddress'
