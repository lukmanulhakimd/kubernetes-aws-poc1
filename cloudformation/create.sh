#!/bin/bash

set -o nounset
set -o pipefail


bastion_up() {
  echo ""
  echo "############################"
  echo "Creating bastion stack.."
  echo "> aws cloudformation create-stack --tag 'Key=pic,Value=$PIC' 'Key=KubernetesCluster,Value=$KUBE_CLUSTER_NAME' --stack-name '${KUBE_CLUSTER_NAME}-bastion' --template-body file://bastion.yml"
  aws cloudformation create-stack --tag "Key=pic,Value=$PIC" "Key=KubernetesCluster,Value=$KUBE_CLUSTER_NAME" --stack-name ${KUBE_CLUSTER_NAME}-bastion --template-body file://bastion.yml
}

bastion_down() {
  #aws cloudformation create-stack --tag "Key=pic,Value=$PIC" "Key=KubernetesCluster,Value=$KUBE_CLUSTER_NAME" --stack-name ${KUBE_CLUSTER_NAME}-bastion --template-body file://bastion.yml
  aws cloudformation delete-stack --stack-name ${KUBE_CLUSTER_NAME}-bastion
}

controller_up() {
  echo ""
  echo "############################"
  echo "Creating controller stack.."
  echo "> aws cloudformation create-stack --tag 'Key=pic,Value=$PIC' 'Key=KubernetesCluster,Value=$KUBE_CLUSTER_NAME' --stack-name ${KUBE_CLUSTER_NAME}-controller --template-body file://controller.yml"
  aws cloudformation create-stack --tag "Key=pic,Value=$PIC" "Key=KubernetesCluster,Value=$KUBE_CLUSTER_NAME" --stack-name ${KUBE_CLUSTER_NAME}-controller --template-body file://controller.yml
}

worker_group1_up() {
  echo ""
  echo "############################"
  echo "Creating worker group-1 stack.."
  echo "> aws cloudformation create-stack --tag 'Key=pic,Value=$PIC' 'Key=KubernetesCluster,Value=$KUBE_CLUSTER_NAME' --stack-name '${KUBE_CLUSTER_NAME}-worker-group1' --template-body file://worker_group1.yml"
  aws cloudformation create-stack --tag "Key=pic,Value=$PIC" "Key=KubernetesCluster,Value=$KUBE_CLUSTER_NAME" --stack-name ${KUBE_CLUSTER_NAME}-worker-group1 --template-body file://worker_group1.yml
}

get_worker_id() {
  aws ec2 describe-instances --filters "Name=tag:aws:cloudformation:stack-name,Values=$KUBE_CLUSTER_NAME" --query "Reservations[*].Instances[*].InstanceId[]" | jq .
}


component=$1
if [[ "${component}" == "controller_up" ]]; then
  echo "Creating controller nodes.."
  controller_up
elif [[ "${component}" == "worker_group1_up" ]]; then
  echo "Creating controller nodes.."
  worker_group1_up
elif [[ "${component}" == "bastion_up" ]]; then
  echo "Creating bastion node.."
  bastion_up
elif [[ "${component}" == "bastion_down" ]]; then
  echo "Deleting bastion node.."
  bastion_down
elif [[ "${component}" == "" ]]; then
  echo ""
else
  echo "${component} provision is not supported!"
fi
