---
# Source: vitess/templates/vitess.yaml
# create a single vttablet service
# set tuple values to more recognizable variables
apiVersion: v1
kind: Service
metadata:
  name: vttablet
  labels:
    app: vitess
  annotations:
    service.alpha.kubernetes.io/tolerate-unready-endpoints: "true"
spec:
  publishNotReadyAddresses: true
  ports:
    - port: 15002
      name: web
    - port: 16002
      name: grpc

  clusterIP: None
  selector:
    app: vitess
    component: vttablet
---
# Source: vitess/templates/vitess.yaml
# set tuple values to more recognizable variables# sanitize inputs to create tablet name# define images to use###################################
# vttablet StatefulSet
###################################
apiVersion: apps/v1beta1
kind: StatefulSet
metadata:
  name: "{{cellname}}-{{keyspace}}-{{shard}}-{{vttablet_type}}"
spec:
  serviceName: vttablet
  replicas: {{vttablet_replicas}}
  podManagementPolicy: Parallel
  updateStrategy: 
    type: RollingUpdate
  selector:
    matchLabels:
      app: vitess
      component: vttablet
      cell: "{{cellname}}"
      keyspace: "{{keyspace}}"
      shard: "{{shard}}"
      type: "{{vttablet_type}}"
  template:
    metadata:
      labels:
        app: vitess
        component: vttablet
        cell: "{{cellname}}"
        keyspace: "{{keyspace}}"
        shard: "{{shard}}"
        type: "{{vttablet_type}}"
    spec:
      terminationGracePeriodSeconds: 3
      securityContext:
        runAsUser: 1000
        fsGroup: 2000
      # set tuple values to more recognizable variables# affinity pod spec
      affinity:
        
      
        podAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          # prefer to be scheduled with same-cell vtgates
          - weight: 10
            podAffinityTerm:
              topologyKey: kubernetes.io/hostname
              labelSelector:
                matchLabels:
                  app: "vitess"
                  component: "vtgate"
                  cell: "{{cellname}}"
      
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          # strongly prefer to stay away from same shard vttablets
          - weight: 100
            podAffinityTerm:
              topologyKey: kubernetes.io/hostname
              labelSelector:
                matchLabels:
                  app: "vitess"
                  component: "vttablet"
                  cell: "{{cellname}}"
                  keyspace: "{{keyspace}}"
                  shard: "{{shard}}"
          
          # prefer to stay away from any vttablets
          - weight: 10
            podAffinityTerm:
              topologyKey: kubernetes.io/hostname
              labelSelector:
                matchLabels:
                  app: "vitess"
                  component: "vttablet"

      initContainers:
        - name: "init-mysql"
          image: "vitess/mysqlctld:latest"
          imagePullPolicy: IfNotPresent
          volumeMounts:
            - name: vtdataroot
              mountPath: "/vtdataroot"
            - name: vt
              mountPath: "/vttmp"
        
          command: ["bash"]
          args:
            - "-c"
            - |
              set -ex
              # set up the directories vitess needs
              mkdir -p /vttmp/bin
              mkdir -p /vtdataroot/tabletdata
        
              # copy necessary assets to the volumeMounts
              cp /vt/bin/mysqlctld /vttmp/bin/
              cp -R /vt/config /vttmp/
        - name: init-vttablet
          image: "vitess/vtctl:latest"
          volumeMounts:
            - name: vtdataroot
              mountPath: "/vtdataroot"
          command: ["bash"]
          args:
            - "-c"
            - |
              set -ex
              # Split pod name (via hostname) into prefix and ordinal index.
              hostname=$(hostname -s)
              [[ $hostname =~ ^(.+)-([0-9]+)$ ]] || exit 1
              pod_prefix=${BASH_REMATCH[1]}
              pod_index=${BASH_REMATCH[2]}
              # Prepend cell name since tablet UIDs must be globally unique.
              uid_name={{cellname}}-$pod_prefix
              # Take MD5 hash of cellname-podprefix.
              uid_hash=$(echo -n $uid_name | md5sum | awk "{print \$1}")
              # Take first 24 bits of hash, convert to decimal.
              # Shift left 2 decimal digits, add in index.
              tablet_uid=$((16#${uid_hash:0:6} * 100 + $pod_index))
              # Save UID for other containers to read.
              echo $tablet_uid > /vtdataroot/tabletdata/tablet-uid
              # Tell MySQL what hostname to report in SHOW SLAVE HOSTS.
              echo report-host=$hostname.vttablet > /vtdataroot/tabletdata/report-host.cnf
              # Orchestrator looks there, so it should match -tablet_hostname above.
        
              # make sure that etcd is initialized
              eval exec /vt/bin/vtctl $(cat <<END_OF_COMMAND
                -topo_implementation="etcd2"
                -topo_global_root=/vitess/global
                -topo_global_server_address="etcd-vitess-client.{{namespace}}:2379"
                -logtostderr=true
                -stderrthreshold=0
                UpdateCellInfo
                -server_address="etcd-vitess-client.{{namespace}}:2379"
                "{{cellname}}"
              END_OF_COMMAND
              )

      containers:
        - name: mysql
          image: "percona:5.7.20"
          imagePullPolicy: Always
          readinessProbe:
            exec:
              command: ["mysqladmin", "ping", "-uroot", "--socket=/vtdataroot/tabletdata/mysql.sock"]
            initialDelaySeconds: 60
            timeoutSeconds: 10
        
          volumeMounts:
            - name: vtdataroot
              mountPath: /vtdataroot
            - name: vt
              mountPath: /vt
          resources:
              limits:
                cpu: 500m
                memory: 1Gi
              requests:
                cpu: 100m
                memory: 512Mi
          env:
            - name: VTROOT
              value: "/vt"
            - name: VTDATAROOT
              value: "/vtdataroot"
            - name: GOBIN
              value: "/vt/bin"
            - name: VT_MYSQL_ROOT
              value: "/usr"
            - name: PKG_CONFIG_PATH
              value: "/vt/lib"
        
            - name: VT_DB_FLAVOR
              valueFrom:
                configMapKeyRef:
                  name: vitess-cm
                  key: db.flavor
        
          command: ["bash"]
          args:
            - "-c"
            - |
              set -ex
        
              if [ "$VT_DB_FLAVOR" = "percona" ]; then
                FLAVOR_MYCNF=/vt/config/mycnf/master_mysql56.cnf
              
              elif [ "$VT_DB_FLAVOR" = "mysql" ]; then
                FLAVOR_MYCNF=/vt/config/mycnf/master_mysql56.cnf
              
              elif [ "$VT_DB_FLAVOR" = "maria" ]; then
                FLAVOR_MYCNF=/vt/config/mycnf/master_mariadb.cnf
              
              fi
              
              export EXTRA_MY_CNF="$FLAVOR_MYCNF:/vtdataroot/tabletdata/report-host.cnf:/vt/config/mycnf/rbr.cnf"
        
              eval exec /vt/bin/mysqlctld $(cat <<END_OF_COMMAND
                -logtostderr=true
                -stderrthreshold=0
                -tablet_dir "tabletdata"
                -tablet_uid "$(cat /vtdataroot/tabletdata/tablet-uid)"
                -socket_file "/vtdataroot/mysqlctl.sock"
                -db-config-dba-uname "vt_dba"
                -db-config-dba-charset "utf8"
                -init_db_sql_file "/vt/config/init_db.sql"
        
              END_OF_COMMAND
              )
        - name: vttablet
          image: "vitess/vttablet:latest"
          readinessProbe:
            httpGet:
              path: /debug/health
              port: 15002
            initialDelaySeconds: 60
            timeoutSeconds: 10
          livenessProbe:
            httpGet:
              path: /debug/status
              port: 15002
            initialDelaySeconds: 60
            timeoutSeconds: 10
          volumeMounts:
            - name: vtdataroot
              mountPath: "/vtdataroot"
          resources:
              limits:
                cpu: 500m
                memory: 1Gi
              requests:
                cpu: 100m
                memory: 512Mi
          ports:
            - name: web
              containerPort: 15002
            - name: grpc
              containerPort: 16002
          env:
            - name: VTROOT
              value: "/vt"
            - name: VTDATAROOT
              value: "/vtdataroot"
            - name: GOBIN
              value: "/vt/bin"
            - name: VT_MYSQL_ROOT
              value: "/usr"
            - name: PKG_CONFIG_PATH
              value: "/vt/lib"
            
        
            - name: VT_DB_FLAVOR
              valueFrom:
                configMapKeyRef:
                  name: vitess-cm
                  key: db.flavor
        
          command: ["bash"]
          args:
            - "-c"
            - |
              set -ex
        
              if [ "$VT_DB_FLAVOR" = "percona" ]; then
                FLAVOR_MYCNF=/vt/config/mycnf/master_mysql56.cnf
              
              elif [ "$VT_DB_FLAVOR" = "mysql" ]; then
                FLAVOR_MYCNF=/vt/config/mycnf/master_mysql56.cnf
              
              elif [ "$VT_DB_FLAVOR" = "maria" ]; then
                FLAVOR_MYCNF=/vt/config/mycnf/master_mariadb.cnf
              
              fi
              
              export EXTRA_MY_CNF="$FLAVOR_MYCNF:/vtdataroot/tabletdata/report-host.cnf:/vt/config/mycnf/rbr.cnf"
              
              
              eval exec /vt/bin/vttablet $(cat <<END_OF_COMMAND
                -topo_implementation="etcd2"
                -topo_global_server_address="etcd-vitess-client.{{namespace}}:2379"
                -topo_global_root=/vitess/global
                -logtostderr
                -port 15002
                -grpc_port 16002
                -service_map "grpc-queryservice,grpc-tabletmanager,grpc-updatestream"
                -tablet_dir "tabletdata"
                -tablet-path "{{cellname}}-$(cat /vtdataroot/tabletdata/tablet-uid)"
                -tablet_hostname "$(hostname).vttablet"
                -init_keyspace "{{keyspace}}"
                -init_shard "{{ init_shard | replace ('x', '') | quote }}"
                -init_tablet_type "{{vttablet_type}}"
                -health_check_interval "5s"
                -mysqlctl_socket "/vtdataroot/mysqlctl.sock"
                -db-config-app-uname "vt_app"
                -db-config-app-dbname "vt_{{keyspace}}"
                -db-config-app-charset "utf8"
                -db-config-dba-uname "vt_dba"
                -db-config-dba-dbname "vt_{{keyspace}}"
                -db-config-dba-charset "utf8"
                -db-config-repl-uname "vt_repl"
                -db-config-repl-dbname "vt_{{keyspace}}"
                -db-config-repl-charset "utf8"
                -db-config-filtered-uname "vt_filtered"
                -db-config-filtered-dbname "vt_{{keyspace}}"
                -db-config-filtered-charset "utf8"
                -enable_replication_reporter
        
        
        
                
                
                  
                
                
              END_OF_COMMAND
              )
        - name: error-log
          image: busybox
          command: ["/bin/sh"]
          args: ["-c", "tail -n+1 -F /vtdataroot/tabletdata/error.log"]
          volumeMounts:
            - name: vtdataroot
              mountPath: /vtdataroot
        - name: slow-log
          image: busybox
          command: ["/bin/sh"]
          args: ["-c", "tail -n+1 -F /vtdataroot/tabletdata/slow.log"]
          volumeMounts:
            - name: vtdataroot
              mountPath: /vtdataroot


      volumes:
        - name: vt
          emptyDir: {}
        

  volumeClaimTemplates:
    - metadata:
        name: vtdataroot
        annotations:
          null
          
      spec:
        accessModes:
        - ReadWriteOnce
        resources:
          requests:
            storage: {{storageSize}}
        storageClassName: {{storageClass}}
---
# Source: vitess/templates/vitess.yaml
###################################
# vttablet PodDisruptionBudget
###################################
apiVersion: policy/v1beta1
kind: PodDisruptionBudget
metadata:
  name: "{{cellname}}-{{keyspace}}-{{shard}}-{{vttablet_type}}"
spec:
  maxUnavailable: 1
  selector:
    matchLabels:
      app: vitess
      component: vttablet
      cell: "{{cellname}}"
      keyspace: "{{keyspace}}"
      shard: "{{shard}}"
      type: "{{vttablet_type}}"

