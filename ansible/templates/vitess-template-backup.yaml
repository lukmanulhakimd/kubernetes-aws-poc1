---
# Source: vitess/templates/vitess.yaml
# shared ConfigMap
apiVersion: v1
kind: ConfigMap
metadata:
  name: vitess-cm
data:
  backup.backup_storage_implementation: s3
  backup.s3_backup_aws_region: ap-southeast-1
  backup.s3_backup_storage_bucket: bukalapak-backup
  backup.s3_backup_storage_root: {{s3_backup_storage_root}}
  backup.s3_backup_server_side_encryption: AES256

  db.flavor: percona
 # end with config
---
# Source: vitess/templates/vitess.yaml
# create a pool of vtgates per cell
# set tuple values to more recognizable variables# define image to use###################################
# vtgate Service
###################################
kind: Service
apiVersion: v1
metadata:
  name: vtgate-{{cellname}}
  labels:
    component: vtgate
    cell: {{cellname}}
    app: vitess
spec:
  ports:
    - name: web
      port: 15001
    - name: grpc
      port: 15991

    - name: mysql
      port: 3306

  selector:
    component: vtgate
    cell: {{cellname}}
    app: vitess
  type: ClusterIP
---
# Source: vitess/templates/vitess.yaml
# create one controller per cell
# set tuple values to more recognizable variables# define image to use###################################
# vtctld Service
###################################
kind: Service
apiVersion: v1
metadata:
  name: vtctld
  labels:
    component: vtctld
    app: vitess
spec:
  ports:
    - name: web
      port: 15000
    - name: grpc
      port: 15999
  selector:
    component: vtctld
    app: vitess
  type: ClusterIP
---
# Source: vitess/templates/vitess.yaml
###################################
# vtgate Deployment
###################################
apiVersion: apps/v1beta1
kind: Deployment
metadata:
  name: vtgate-{{cellname}}
spec:
  replicas: {{vtgate_replicas}}
  selector:
    matchLabels:
      app: vitess
      component: vtgate
      cell: {{cellname}}
  template:
    metadata:
      labels:
        app: vitess
        component: vtgate
        cell: {{cellname}}
    spec:
      terminationGracePeriodSeconds: 3
      securityContext:
        runAsUser: 1000
        fsGroup: 2000
      # set tuple values to more recognizable variables# affinity pod spec
      affinity:
        
      
        podAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          # prefer to be scheduled with same-cell vttablets
          - weight: 10
            podAffinityTerm:
              topologyKey: kubernetes.io/hostname
              labelSelector:
                matchLabels:
                  app: "vitess"
                  component: "vttablet"
                  cell: "{{cellname}}"
      
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          # prefer to stay away from other same-cell vtgates
          - weight: 10
            podAffinityTerm:
              topologyKey: kubernetes.io/hostname
              labelSelector:
                matchLabels:
                  app: "vitess"
                  component: "vtgate"
                  cell: "{{cellname}}"


      initContainers:
        - name: init-mysql-creds
          image: "vitess/vtgate:latest"
          volumeMounts:
            - name: creds
              mountPath: "/mysqlcreds"
          env:
            - name: MYSQL_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: {{cellname}}-{{keyspace}}-password
                  key: password
        
          command: ["bash"]
          args:
            - "-c"
            - |
              set -ex
              creds=$(cat <<END_OF_COMMAND
              {
                "root": [
                  {
                    "UserData": "root",
                    "Password": "$MYSQL_PASSWORD"
                  }
                ],
                "vt_appdebug": []
              }
              END_OF_COMMAND
              )
              echo $creds > /mysqlcreds/creds.json


      containers:
        - name: vtgate
          image: vitess/vtgate:latest
          readinessProbe:
            httpGet:
              path: /debug/health
              port: 15001
            initialDelaySeconds: 30
            timeoutSeconds: 5
          livenessProbe:
            httpGet:
              path: /debug/status
              port: 15001
            initialDelaySeconds: 30
            timeoutSeconds: 5
          volumeMounts:
            - name: creds
              mountPath: "/mysqlcreds"

          resources:
            limits:
              cpu: {{vtgate_cpu_limit}}
              memory: {{vtgate_memory_limit}}
            requests:
              cpu: {{vtgate_cpu_request}}
              memory: {{vtgate_memory_request}}

            

          command:
            - bash
            - "-c"
            - |
              set -ex

              eval exec /vt/bin/vtgate $(cat <<END_OF_COMMAND
                -topo_implementation=etcd2
                -topo_global_server_address="etcd-vitess-client.{{namespace}}:2379"
                -topo_global_root=/vitess/global
                -logtostderr=true
                -stderrthreshold=0
                -port=15001
                -grpc_port=15991

                -mysql_server_port=3306
                -mysql_auth_server_static_file="/mysqlcreds/creds.json"

                -service_map="grpc-vtgateservice"
                -cells_to_watch="{{cellname}}"
                -tablet_types_to_wait="MASTER,REPLICA"
                -gateway_implementation="discoverygateway"
                -cell="{{cellname}}"
                
              END_OF_COMMAND
              )
      volumes:
        - name: creds
          emptyDir: {}
---
# Source: vitess/templates/vitess.yaml
###################################
# vtctld Service + Deployment
###################################
apiVersion: apps/v1beta1
kind: Deployment
metadata:
  name: vtctld
spec:
  replicas: {{vtctld_replicas}}
  selector:
    matchLabels:
      app: vitess
      component: vtctld
  template:
    metadata:
      labels:
        app: vitess
        component: vtctld
    spec:
      securityContext:
        runAsUser: 1000
        fsGroup: 2000
      # set tuple values to more recognizable variables
      containers:
        - name: vtctld
          image: vitess/vtctld:latest
          readinessProbe:
            httpGet:
              path: /debug/health
              port: 15000
            initialDelaySeconds: 30
            timeoutSeconds: 5
          livenessProbe:
            httpGet:
              path: /debug/status
              port: 15000
            initialDelaySeconds: 30
            timeoutSeconds: 5
          env:
            - name: VT_BACKUP_SERVICE
              valueFrom:
                configMapKeyRef:
                  name: vitess-cm
                  key: backup.backup_storage_implementation
            - name: VT_S3_BACKUP_AWS_REGION
              valueFrom:
                configMapKeyRef:
                  name: vitess-cm
                  key: backup.s3_backup_aws_region
            - name: VT_S3_BACKUP_STORAGE_BUCKET
              valueFrom:
                configMapKeyRef:
                  name: vitess-cm
                  key: backup.s3_backup_storage_bucket
            - name: VT_S3_BACKUP_STORAGE_ROOT
              valueFrom:
                configMapKeyRef:
                  name: vitess-cm
                  key: backup.s3_backup_storage_root
            - name: VT_S3_BACKUP_SERVER_SIDE_ENCRYPTION
              valueFrom:
                configMapKeyRef:
                  name: vitess-cm
                  key: backup.s3_backup_server_side_encryption
          volumeMounts:
            - name: backup-creds
              mountPath: /etc/secrets/creds
          resources:
            limits:
              cpu: {{vtctld_cpu_limit}}
              memory: {{vtctld_memory_limit}}
            requests:
              cpu: {{vtctld_cpu_request}}
              memory: {{vtctld_memory_request}}
            
          command:
            - bash
            - "-c"
            - |
              set -ex;

              credsPath=/etc/secrets/creds/$(ls /etc/secrets/creds/ | head -1)
              export AWS_SHARED_CREDENTIALS_FILE=$credsPath
              cat $AWS_SHARED_CREDENTIALS_FILE

              eval exec /vt/bin/vtctld $(cat <<END_OF_COMMAND
                -cell="{{cellname}}"
                -web_dir="/vt/web/vtctld"
                -web_dir2="/vt/web/vtctld2/app"
                -workflow_manager_init
                -workflow_manager_use_election
                -logtostderr=true
                -stderrthreshold=0
                -port=15000
                -grpc_port=15999
                -service_map="grpc-vtctl"
                -topo_implementation="etcd2"
                -topo_global_server_address="etcd-vitess-client.{{namespace}}:2379"
                -topo_global_root=/vitess/global

                -backup_storage_implementation=$VT_BACKUP_SERVICE
                -s3_backup_aws_region=$VT_S3_BACKUP_AWS_REGION
                -s3_backup_storage_bucket=$VT_S3_BACKUP_STORAGE_BUCKET
                -s3_backup_storage_root=$VT_S3_BACKUP_STORAGE_ROOT
                -s3_backup_server_side_encryption=$VT_S3_BACKUP_SERVER_SIDE_ENCRYPTION

              END_OF_COMMAND
              )

      volumes:
        - name: backup-creds
          secret:
            secretName: bukalapakbackup-s3-creds

---
# Source: vitess/templates/vitess.yaml
###################################
# vtgate PodDisruptionBudget
###################################
apiVersion: policy/v1beta1
kind: PodDisruptionBudget
metadata:
  name: vtgate-{{cellname}}
spec:
  maxUnavailable: 1
  selector:
    matchLabels:
      app: vitess
      component: vtgate
      cell: {{cellname}}
